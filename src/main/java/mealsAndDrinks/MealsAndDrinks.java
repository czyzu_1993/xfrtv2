package mealsAndDrinks;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by kuba on 12.03.16.
 */
@Entity
public abstract class MealsAndDrinks {
    private long id;
    private String name;
    private double price;

    public MealsAndDrinks(double price, String name) {
        this.price = price;
        this.name = name;
    }

    public MealsAndDrinks(MealsAndDrinks mealsAndDrinks) {
        this.price = mealsAndDrinks.price;
        this.name = mealsAndDrinks.name;
    }

    protected MealsAndDrinks() {
    }

    @Id
    @GeneratedValue
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
