package mealsAndDrinks;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * Created by kuba on 12.03.16.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Dessert extends MealsAndDrinks {

    public Dessert(double price, String name) {
        super(price, name);
    }

    public Dessert() {
    }
}
