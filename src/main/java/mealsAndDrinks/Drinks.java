package mealsAndDrinks;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * Created by kuba on 12.03.16.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Drinks extends MealsAndDrinks {
    boolean iceCubes;
    boolean lemon;

    public Drinks(double price, String name) {
        super(price, name);
        iceCubes = false;
        lemon = false;
    }

    public Drinks(Drinks drinks) {
        super(drinks);
        iceCubes = drinks.iceCubes;
        lemon = drinks.lemon;
    }

    public Drinks() {
    }

    public void setIceCubes(boolean iceCubes) {
        this.iceCubes = iceCubes;
    }

    public void setLemon(boolean lemon) {
        this.lemon = lemon;
    }

}
