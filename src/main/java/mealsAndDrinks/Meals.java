package mealsAndDrinks;

import enumeration.Cuisines;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;


/**
 * Created by kuba on 12.03.16.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Meals extends MealsAndDrinks {
    private Cuisines cuisines;

    public Meals(double price, String name, Cuisines cuisines) {
        super(price, name);
        this.cuisines = cuisines;
    }

    public Meals() {
    }

    public Cuisines getCuisines() {
        return cuisines;
    }

    public void setCuisines(Cuisines cuisines) {
        this.cuisines = cuisines;
    }
}
