package mineSweeper;

/**
 * Created by kuba on 13.03.16.
 */
public class MineSweeperImp implements MineSweeper {
    private String mineField;

    @Override
    public void setMineField(String mineField) throws IllegalArgumentException {
        if (!mineField.matches("[\\.\\*\\n]+") || !testString(mineField)) throw new IllegalArgumentException();
        this.mineField = mineField;
    }

    @Override
    public String getHintField() throws IllegalStateException {
        if (mineField == null) throw new IllegalStateException();
        StringBuilder stringBuilder = new StringBuilder();
        String[] parts = mineField.split("\\n");
        char[][] charsTable = new char[parts.length][parts[1].length()];
        for (int i = 0; i < parts.length; i++) {
            parts[i].getChars(0, parts[0].length(), charsTable[i], 0);
        }
        int count = 0;
        for (int i = 0; i < parts.length; i++) {
            for (int j = 0; j < parts[i].length(); j++) {
                if (charsTable[i][j] == '*') stringBuilder.append("*");
                else {
                    if (indexExists(charsTable, i - 1, j - 1)) count++;
                    if (indexExists(charsTable, i - 1, j)) count++;
                    if (indexExists(charsTable, i - 1, j + 1)) count++;
                    if (indexExists(charsTable, i, j - 1)) count++;
                    if (indexExists(charsTable, i, j + 1)) count++;
                    if (indexExists(charsTable, i + 1, j - 1)) count++;
                    if (indexExists(charsTable, i + 1, j)) count++;
                    if (indexExists(charsTable, i + 1, j + 1)) count++;
                    stringBuilder.append(count);
                    count = 0;
                }
            }
            stringBuilder.append("\n");
        }
        return "" + stringBuilder;
    }

    private boolean indexExists(char[][] table, int c, int l) {
        return (c >= 0 && l >= 0 && c < table.length && l < table[0].length && table[c][l] == '*');
    }

    private boolean testString(String s) {
        String[] parts = s.split("\\n");
        for (String str : parts) {
            if (!(str.length() == parts[0].length())) {
                return false;
            }
        }
        return true;
    }
}
