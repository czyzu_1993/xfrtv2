package orderAndCustomer;

import mealsAndDrinks.MealsAndDrinks;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuba on 12.03.16.
 */
public class Customer {
    private List<SingleOrder> ordersList;

    public Customer() {
        this.ordersList = new ArrayList<>();
    }

    public void addOrder(int count, MealsAndDrinks mealsAndDrinks) {
        ordersList.add(new SingleOrder(count, mealsAndDrinks));
    }

    public double gerReceipt() {
        double price = 0;
        for (SingleOrder s : ordersList) {
            price = price + s.getCount() * s.getMealsAndDrinks().getPrice();
        }
        return price;
    }
}
