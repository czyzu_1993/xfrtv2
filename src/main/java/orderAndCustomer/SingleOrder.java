package orderAndCustomer;

import mealsAndDrinks.MealsAndDrinks;

/**
 * Created by kuba on 12.03.16.
 */
public class SingleOrder {
    private int count;
    private MealsAndDrinks mealsAndDrinks;

    public SingleOrder(int count, MealsAndDrinks mealsAndDrinks) {
        this.count = count;
        this.mealsAndDrinks = mealsAndDrinks;
    }

    public int getCount() {
        return count;
    }

    public MealsAndDrinks getMealsAndDrinks() {
        return mealsAndDrinks;
    }
}
