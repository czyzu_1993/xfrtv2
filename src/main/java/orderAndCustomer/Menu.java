package orderAndCustomer;

import enumeration.Cuisines;
import mealsAndDrinks.Dessert;
import mealsAndDrinks.Drinks;
import mealsAndDrinks.Meals;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by kuba on 12.03.16.
 */

@Repository
@Transactional
public class Menu {
    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void init() { //Initialization only because hibernate.hbm2ddl.auto is in create mode
        Session session = sessionFactory.getCurrentSession();
        session.persist(new Drinks(10, "Tee"));
        session.persist(new Drinks(8, "White coffee"));
        session.persist(new Drinks(7, "Black Coffee"));
        session.persist(new Drinks(4, "Water"));
        session.persist(new Drinks(10, "Orange juice"));
        session.persist(new Drinks(10, "Apple juice"));

        session.persist(new Dessert(10, "Cake"));
        session.persist(new Dessert(10, "Apple"));
        session.persist(new Dessert(10, "Jelly"));
        session.persist(new Dessert(10, "Ice creams"));

        session.persist(new Meals(20, "Tomato Soup", Cuisines.POLISH));
        session.persist(new Meals(44, "Lasagne", Cuisines.ITALIAN));
        session.persist(new Meals(42, "Burrito", Cuisines.MEXICAN));
    }

    @SuppressWarnings("unchecked")
    public List<Dessert> getDessertMenu() {
        List<Dessert> dessertMenu = sessionFactory.getCurrentSession().createQuery("select d from Dessert d").list();
        for (int i = 0; i < dessertMenu.size(); i++) {
            System.out.println(i + " " + dessertMenu.get(i).getName() + " " + dessertMenu.get(i).getPrice());
        }
        return dessertMenu;
    }

    @SuppressWarnings("unchecked")
    public List<Drinks> getDrinkMenu() {
        List<Drinks> drinkMenu = sessionFactory.getCurrentSession().createQuery("select d from Drinks d").list();
        for (int i = 0; i < drinkMenu.size(); i++) {
            System.out.println(i + " " + drinkMenu.get(i).getName() + " " + drinkMenu.get(i).getPrice());
        }
        return drinkMenu;
    }

    @SuppressWarnings("unchecked")
    public List<Meals> getMainMenu() {
        List<Meals> mainMenu = sessionFactory.getCurrentSession().createQuery("select m from Meals m order by cuisines").list();
        for (int i = 0; i < mainMenu.size(); i++) {
            System.out.println(i + " " + mainMenu.get(i).getName() + " " + mainMenu.get(i).getCuisines() + " " + mainMenu.get(i).getPrice());
        }
        return mainMenu;
    }
}
